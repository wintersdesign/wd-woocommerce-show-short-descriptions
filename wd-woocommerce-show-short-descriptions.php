<?php
/**
 * @wordpress-plugin
 * Plugin Name:       Show Short Descriptions for WooCommerce
 * Plugin URI:        https://bitbucket.org/wintersdesign/woocommerce-show-short-descriptions
 * Description:       Display Product Short Descriptions in WooCommerce Archive pages
 * Version:           1.0.0
 * Author:            Frankie Winters
 * Author URI:        winters.design
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce-show-short-descriptions
 * Domain Path:       /languages
 * Plugin URI: http://code.tutsplus.com/tutorials/woocommerce-adding-the-product-short-description-to-archive-pages--cms-25435
 * Description: Add product short descriptions to the loop in product archive pages (requires WooCommerce to be activated)
 * Version: 1.0
 * Author: Rachel McCollin
 * Author URI: http://rachelmccollin.co.uk
 *
 */

add_action( 'woocommerce_after_shop_loop_item_title', 'wintersdesign_excerpt_after_shop_loop_item_title', 40 );

function wintersdesign_excerpt_after_shop_loop_item_title() {
  the_excerpt();
}
